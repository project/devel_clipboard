CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This is a light weight module to reduce the developer's time from manual code
type, copy/paste etc. This module will keep the devel executed php code and
display in select list. Once you choose the code it will be pasted automatically
in text area.

For a full description of the module, visit the project page:
https://www.drupal.org/project/devel_clipboard


REQUIREMENTS
------------

 * No Additional Requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8
   /installing-drupal-8-modules for further information.


CONFIGURATION
-------------

 * Private folder need to be configured properly.


MAINTAINERS
-----------

Current maintainers:
 * Ameer Khan - https://www.drupal.org/u/ameer-khan
